addEventListener('scroll', () => {
    const header = document.querySelector('header');
    header.classList.toggle('sticky', scrollY > 0);
});

document.querySelector('#year').textContent = new Date().getFullYear();

/***
 * Menu
 */
const menuBurger    = document.querySelector('.menu-burger');
const menuClose     = document.querySelector('.menu-close');
const navigation    = document.querySelector('.navigation');  

const iconMenu = document.querySelector('.icon-menu');

iconMenu.addEventListener('click', () => {
    if(menuBurger.classList.contains('active')) {
        menuBurger.classList.remove('active');
        menuBurger.style.display = 'none';
        menuClose.style.display = 'block';
        navigation.style.left = '0';
    } else {
        menuBurger.classList.add('active');
        menuBurger.style.display = 'block';
        menuClose.style.display = 'none';
        navigation.style.left = '-100%';
    }
});

/***
 * Navigation
 */
let links = document.querySelectorAll('.navigation > li a');
links.forEach((link) => {
    link.addEventListener('click', () => {
        menuBurger.classList.add('active');
        menuBurger.style.display = 'block';
        menuClose.style.display = 'none';
        navigation.style.left = '-100%';
    });
});